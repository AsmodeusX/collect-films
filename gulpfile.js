"use strict";

// Load plugins
const autoprefixer = require("autoprefixer");
const cp = require("child_process");
const cssnano = require("cssnano");
const del = require("del");
const gulp = require("gulp");
const imagemin = require("gulp-imagemin");
const plumber = require("gulp-plumber");
const postcss = require("gulp-postcss");
const rename = require("gulp-rename");
const uglify = require('gulp-uglify');
const sass = require("gulp-sass");
// const browsersync = require("browser-sync");
const concat = require("gulp-concat");
let terser = require('gulp-terser');

// BrowserSync
// function browserSync(done) {
//     browsersync.init({
//         notify: true,
//         port: 8000,
//         proxy: 'localhost:8000'
//     });
//     done();
// }

// // BrowserSync Reload
// function browserSyncReload(done) {
//     browsersync.reload();
//     done();
// }


// Clean assets
function clean() {
    return del(["./src/dist/"]);
}

// Optimize Images
function swf() {
    return gulp
        .src("./src/assets/swf/**/*")
        .pipe(gulp.dest("./src/dist/swf"));
}

// Optimize Images
function images() {
    return gulp
        .src("./src/assets/img/**/*")
        .pipe(gulp.dest("./src/dist/images"));
}

// Optimize Images
function images_compress() {
    return gulp
        .src("./src/assets/img/**/*")
        .pipe(
            imagemin([
                imagemin.gifsicle({ interlaced: true }),
                imagemin.jpegtran({ progressive: true }),
                imagemin.optipng({ optimizationLevel: 5 }),
                imagemin.svgo({
                    plugins: [
                        {
                            removeViewBox: false,
                            collapseGroups: true
                        }
                    ]
                })
            ])
        )
        .pipe(gulp.dest("./src/dist/images"));
}

// SCSS task
function scss() {
    return gulp
        .src("./src/assets/scss/**/*.scss")
        .pipe(plumber())
        .pipe(sass({ outputStyle: "expanded" }))
        .pipe(gulp.dest("./src/dist/styles/"))
        .pipe(rename({ suffix: ".min" }))
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(gulp.dest("./src/dist/styles/"))
}

// CSS task
function styles_default() {
    return gulp
        .src([
            "./src/assets/css/reset.css",
            "./src/assets/css/bootstrap.css",
            "./src/assets/css/bootstrap-grid.css",
            "./src/assets/css/jquery-ui.css",
            "./node_modules/swiper/swiper-bundle.css",
        ])
        .pipe(concat("default.min.css"))
        .pipe(plumber())
        .pipe(sass({ outputStyle: "expanded" }))
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(gulp.dest("./src/dist/styles/"))
}

function styles_global() {
    return gulp
        .src([
            "./src/assets/scss/base_popup.scss",
            "./src/assets/scss/subscribes/popups/subscribe.scss",
            "./src/assets/scss/subscribes/block.scss",
        ])
        .pipe(plumber())
        .pipe(sass({ outputStyle: "expanded" }))
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(concat("global.min.css"))
        .pipe(gulp.dest("./src/dist/styles/"))
}

function styles_global_critical() {
    return gulp
        .src([
            "./src/assets/scss/global.scss",
            "./src/assets/scss/components/menu/menu.scss",
            "./src/assets/scss/components/header/header.scss",
            "./src/assets/scss/forms.scss",
        ])
        .pipe(plumber())
        .pipe(sass({ outputStyle: "expanded" }))
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(concat("global_critical.min.css"))
        .pipe(gulp.dest("./src/dist/styles/"))
}

function styles_main_page() {
    return gulp
        .src([
            "./src/assets/scss/films/blocks/recommend.scss",
            "./src/assets/scss/films/blocks/bestsellers.scss",
            "./src/assets/scss/films/blocks/news.scss",
            "./src/assets/scss/films/blocks/popular.scss",
            "./src/assets/scss/films/components/top.scss",
            "./src/assets/scss/films/components/film.scss",
            "./src/assets/scss/films/components/rating.scss",
            "./src/assets/scss/main/page.scss"
        ])
        .pipe(plumber())
        .pipe(sass({ outputStyle: "expanded" }))
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(concat("main.min.css"))
        .pipe(gulp.dest("./src/dist/styles/pages/"))
}


function styles_genres_page() {
    return gulp
        .src([
            "./src/assets/scss/films/components/genre.scss",
            "./src/assets/scss/films/genres.scss",
        ])
        .pipe(plumber())
        .pipe(sass({ outputStyle: "expanded" }))
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(concat("genres.min.css"))
        .pipe(gulp.dest("./src/dist/styles/pages/"))
}


function styles_genre_page() {
    return gulp
        .src([
            "./src/assets/scss/films/blocks/recommend.scss",
            "./src/assets/scss/films/blocks/bestsellers.scss",
            "./src/assets/scss/films/blocks/news.scss",
            "./src/assets/scss/films/blocks/popular.scss",
            "./src/assets/scss/films/components/top.scss",
            "./src/assets/scss/films/components/film.scss",
            "./src/assets/scss/films/components/rating.scss",
            "./src/assets/scss/films/genre.scss"
        ])
        .pipe(plumber())
        .pipe(sass({ outputStyle: "expanded" }))
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(concat("genre.min.css"))
        .pipe(gulp.dest("./src/dist/styles/pages/"))
}


function styles_film_page() {
    return gulp
        .src([
            "./src/assets/scss/films/film.scss",
            "./src/assets/scss/films/blocks/recommend.scss",
            "./src/assets/scss/films/blocks/bestsellers.scss",
            "./src/assets/scss/films/blocks/news.scss",
            "./src/assets/scss/films/blocks/popular.scss",
            "./src/assets/scss/films/components/top.scss",
            "./src/assets/scss/films/components/film.scss",
            "./src/assets/scss/films/components/rating.scss",
        ])
        .pipe(plumber())
        .pipe(sass({ outputStyle: "expanded" }))
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(concat("film.min.css"))
        .pipe(gulp.dest("./src/dist/styles/pages/"))
}


function styles_search_page() {
    return gulp
        .src([
            "./src/assets/scss/films/blocks/recommend.scss",
            "./src/assets/scss/films/blocks/bestsellers.scss",
            "./src/assets/scss/films/blocks/news.scss",
            "./src/assets/scss/films/blocks/popular.scss",
            "./src/assets/scss/films/components/top.scss",
            "./src/assets/scss/films/components/film.scss",
            "./src/assets/scss/films/components/rating.scss",
            "./src/assets/scss/films/search.scss",
        ])
        .pipe(plumber())
        .pipe(sass({ outputStyle: "expanded" }))
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(concat("search.min.css"))
        .pipe(gulp.dest("./src/dist/styles/pages/"))
}



function scripts_vendors() {
    return (
        gulp
            .src([
                "./bower_components/jquery/dist/jquery.min.js",
                "./bower_components/jquery-ui/jquery-ui.min.js",
                "./bower_components/jquery.cookie/jquery.cookie.js",
                "./src/assets/scripts/bootstrap.min.js",
                "./bower_components/bLazy/blazy.min.js",
                "./node_modules/swiper/swiper-bundle.js",
                "./node_modules/vue/dist/vue.js",
                "./node_modules/axios/dist/axios.min.js",
                "./node_modules/vue-axios/dist/vue-axios.min.js",
            ])
            .pipe(terser())
            .pipe(plumber())
            .pipe(concat("scripts_vendors.min.js"))
            .pipe(gulp.dest("./src/dist/scripts/"))
    );
}

function scripts_global() {
    return (
        gulp
            .src([
                "./src/assets/scripts/inspectors.js",
                "./src/assets/scripts/check_hash.js",
                "./src/assets/scripts/films/search.js",
                "./src/assets/scripts/components/menu/menu.js",
                "./src/assets/scripts/components/header/header.js",
                "./src/assets/scripts/base_popup.js",
                "./src/assets/scripts/subscribes/popups.js",
                "./src/assets/scripts/subscribes/forms.js",
            ])
            .pipe(plumber())
            .pipe(terser())
            .pipe(concat("global.min.js"))
            .pipe(gulp.dest("./src/dist/scripts/"))
    );
}

function scripts_film() {
    return (
        gulp
            .src([
                "./src/assets/scripts/films/film.js",
                // "./src/assets/scripts/playerjs.js",
            ])
            .pipe(plumber())
            .pipe(terser())
            .pipe(concat("film.min.js"))
            .pipe(gulp.dest("./src/dist/scripts/"))
    );
}

function scripts() {
    return (
        gulp
            .src(["./src/assets/scripts/**/*"])
            .pipe(gulp.dest("./src/dist/scripts/"))
    );
}

// Watch files
function watchFiles() {
    gulp.watch("./src/assets/css/**/*", gulp.parallel(styles_default));
    gulp.watch("./src/assets/scss/**/*", gulp.parallel(
        styles_global,
        styles_global_critical,
        styles_main_page,
        styles_genres_page,
        styles_genre_page,
        styles_film_page,
        styles_search_page,
        scss
    ));
    gulp.watch("./src/assets/scripts/**/*", gulp.parallel(scripts_vendors, scripts_global, scripts_film, scripts));
    gulp.watch("./src/assets/swf/**/*", gulp.parallel(swf));
    gulp.watch("./src/assets/img/**/*", images);
    // gulp.watch(
    //     [
    //         "./src/templates/**/*",
    //     ],
    //     gulp.series(browserSyncReload)
    // );
}



// define complex tasks
const js = gulp.series(scripts_vendors, scripts_global, scripts_film, scripts);
const build = gulp.series(clean, scripts_vendors, gulp.parallel(
    styles_default,
    scss,
    styles_global,
    styles_global_critical,
    images,
    styles_main_page,
    styles_genres_page,
    styles_genre_page,
    styles_film_page,
    styles_search_page,
    scripts_global,
    scripts_film,
    scripts,
    images_compress
));
const collect = gulp.series(scripts_vendors, gulp.parallel(
    styles_default,
    scss,
    styles_global,
    styles_global_critical,
    styles_main_page,
    styles_genres_page,
    styles_genre_page,
    styles_film_page,
    styles_search_page,
    images,
    scripts_global,
    scripts_film,
    scripts,
    swf
));

const watch =  gulp.parallel(collect, watchFiles);

// export tasks
exports.images = images;
exports.scss = scss;
exports.js = js;
exports.swf = swf;
exports.clean = clean;
exports.build = build;
exports.watch = watch;
exports.default = collect;
