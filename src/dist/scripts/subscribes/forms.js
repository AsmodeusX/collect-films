(function ($) {
    $(document).on('submit', 'form.form-subscribe', function(e) {
        e.preventDefault();
        var $form = $(e.target);
        $form.find('.error-message').remove();
        sendMessage($form);
    });

    function sendMessage($form) {
        axios.post('/subscribes/ajax/subscribe/', $form.serialize())
            .then((response) => {
                setSubscribeCompleted();
                $.basePopup({
                    closeOnOtherClick: true,
                    addBtnClose: true,
                    content: response.data.popup,
                    classes: 'popup--subscribe-thanks'
                });
                $form.get(0).reset();
            }, (e) => {
                var errors = e.response.data.errors;
                for (var error in errors) {
                    $form.find("[name='" + error + "']").parent().append(
                        $('<div />').addClass('error-message').html(errors[error])
                    );
                    // console.log(error, errors[error]);
                }
                // for (var i = 0; i < errors.length; i++) {
                //     var error = errors[i];
                //     console.log(error);
                //     $form.find("name='" +  + "'")
                // }
            });
    }

    function setSubscribeCompleted() {

    }
})(jQuery);