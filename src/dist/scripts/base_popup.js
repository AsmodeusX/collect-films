(function () {
    $.basePopup = function (get_params) {
        var default_params = {
            closeOnOtherClick: false,
            addBtnClose: false,
            content: '',
            classes: ''
        }

        var params = Object.assign(default_params, get_params);
        _addPopup();

        function _addPopup() {
            _removePopup();
            var $body = $('body');
            $body.append(
                params.content
            );
            $body.addClass('non-scrollable');
            $('.popup').addClass(params.classes);

            if (params.closeOnOtherClick) {
                $(document).on('mouseup', function (e){ // событие клика по веб-документу
                    var $popup = $('.popup__content');
                    if (!$popup.is(e.target) // если клик был не по нашему блоку
                        && $popup.has(e.target).length === 0) { // и не по его дочерним элементам
                        _removePopup();
                    }
                });
            }

            if (params.addBtnClose) {
                var closeBtn = $('<div />').addClass('close-popup');
                $('.popup__content').prepend(closeBtn);
                $('.close-popup').on('click', function () {
                    _removePopup();
                })
            }
        }

        function _removePopup() {
            var $body = $('body');
            $body.find('.popup').remove();
            $body.removeClass('non-scrollable');
            $(document).off('mouseup');
        }

        function removePopup() {
            _removePopup();
        }
    }
})(jQuery);