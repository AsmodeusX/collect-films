(function ($) {
    var clBaseMenu = '.navbar:not(.navbar--fixed)',
        clFixedMenu = '.navbar.navbar--fixed';
    var clVisibleFixedMenu = 'visible';
    var $fixedMenu;

    // $.AddElementCheckVisibility(clBaseMenu);
    //
    // $(document).ready(function () {
    //     $fixedMenu = $(clFixedMenu);
    // })
    //
    // $(window).on('scroll', function () {
    //     if ($fixedMenu && $fixedMenu.length) {
    //
    //         if (!$.stateVisibility(clBaseMenu)) {
    //             $fixedMenu.addClass(clVisibleFixedMenu)
    //         } else {
    //             $fixedMenu.removeClass(clVisibleFixedMenu)
    //         }
    //     }
    // });

    $('.nav-link').on('click', function () {
        setTimeout($.checkHash, 100);
    });

    $(document).on('mouseup', function (e){ // событие клика по веб-документу
        var $popup = $('.navbar--fixed.visible');
        if (!$popup.is(e.target) // если клик был не по нашему блоку
            && $popup.has(e.target).length === 0) { // и не по его дочерним элементам
            $popup.find('.navbar-toggler:not(.collapsed)').trigger('click');
        }
    });

    $(document).on('mouseup', function (e){ // событие клика по веб-документу
        var $popup = $('.navbar');
        if (!$popup.is(e.target) // если клик был не по нашему блоку
            && $popup.has(e.target).length === 0) { // и не по его дочерним элементам
            $popup.find('.navbar-toggler:not(.collapsed)').trigger('click');
        }
    });

    $(document).ready(function () {
        setCollapseMenu();
    });

    function setCollapseMenu() {
        var $collapseMenu = $('#collapseMenu'),
            classMenuOpened = 'menu-opened',
            $body = $('body');

        $collapseMenu
            .on('show.bs.collapse', function () {
                $body.addClass(classMenuOpened);
            })
            .on('hide.bs.collapse', function () {
                $body.removeClass(classMenuOpened);
            });
    }
})(jQuery);