(function ($) {

    $(document).ready(function () {
        setSwiper();
    });

    function setSwiper() {
        var swiper = new Swiper('.swiper-header-container', {
            loop: true,
            slidesPerView: 1,
            slidesPerGroup: 1,
            effect: 'fade',
            fadeEffect: {
                crossFade: true
            },
            speed: 2000,
            autoplay: {
                delay: 5000,
            },
            allowTouchMove: false,
        });
    }

})(jQuery);