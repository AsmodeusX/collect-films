from ipware import get_client_ip


def get_ip(request):
    client_ip, is_routable = get_client_ip(request)

    return client_ip
