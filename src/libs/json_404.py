from django.http import JsonResponse


def json_404(msg=object):
    return JsonResponse(msg, status=404)