from django.contrib import admin
from solo.admin import SingletonModelAdmin


class PageConfigAdmin(SingletonModelAdmin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': (
                'title', 'header',
            ),
        }),
    )


class SEOPostAdmin(admin.ModelAdmin):
    fieldsets = PageConfigAdmin.fieldsets + (
        (None, {
            'fields': (
                'seo_title', 'seo_description', 'seo_keywords',
            ),
        }),
    )
