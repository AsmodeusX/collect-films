from django.db import models
from django.utils.translation import ugettext_lazy as _


class PageConfig(models.Model):
    title = models.CharField(_('title'), max_length=64, default='')
    header = models.CharField(_('header'), max_length=128, default='')

    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        abstract = True
        default_permissions = ('change', )
        verbose_name = _('settings')


class SEOPageConfig(PageConfig):
    seo_title = models.TextField(_('seo title'), blank=True)
    seo_description = models.TextField(_('seo description'), blank=True)
    seo_keywords = models.TextField(_('seo keywords'), blank=True)

    class Meta(PageConfig.Meta):
        abstract = True
