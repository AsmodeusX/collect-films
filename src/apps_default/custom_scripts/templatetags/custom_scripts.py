from django.template import loader, Library
from django.utils.safestring import mark_safe
from ..models import Script
from .. import options

register = Library()


@register.simple_tag(takes_context=True)
def custom_scripts_body_start(context):
    scripts = Script.objects.filter(active=True, position=options.POSITION_BODY_START)
    list_scripts = []
    for script in scripts:
        list_scripts.append(mark_safe(script.code))

    return loader.render_to_string('custom_scripts/scripts.html', {
        'custom_scripts': list_scripts,
    })


@register.simple_tag(takes_context=True)
def custom_scripts_body_end(context):
    scripts = Script.objects.filter(active=True, position=options.POSITION_BODY_END)
    list_scripts = []
    for script in scripts:
        list_scripts.append(mark_safe(script.code))

    return loader.render_to_string('custom_scripts/scripts.html', {
        'custom_scripts': list_scripts,
    })


@register.simple_tag(takes_context=True)
def custom_scripts_head(context):
    scripts = Script.objects.filter(active=True, position=options.POSITION_HEAD)
    list_scripts = []
    for script in scripts:
        list_scripts.append(mark_safe(script.code))

    return loader.render_to_string('custom_scripts/scripts.html', {
        'custom_scripts': list_scripts,
    })
