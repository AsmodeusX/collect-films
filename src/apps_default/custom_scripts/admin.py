from django.contrib import admin
from .models import Script


@admin.register(Script)
class ScriptAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': (
                'name', 'code', 'position', 'active',
            ),
        }),
    )
    list_display = ('name', 'position', 'active', )

