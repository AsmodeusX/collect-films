from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class Config(AppConfig):
    default_auto_field = 'django.db.models.AutoField'
    name = 'custom_scripts'
    verbose_name = _('Custom Scripts')
