from django.utils.translation import ugettext_lazy as _

POSITION_HEAD = 1
POSITION_BODY_START = 2
POSITION_BODY_END = 3

POSITIONS = (
    (POSITION_HEAD, _('HEAD')),
    (POSITION_BODY_START, _('START OF BODY')),
    (POSITION_BODY_END, _('END OF BODY')),
)
