from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class Config(AppConfig):
    default_auto_field = 'django.db.models.AutoField'
    name = 'main'
    verbose_name = _('Home page')

