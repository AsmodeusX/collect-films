from .models import MainPageConfig
from django.views.generic import TemplateView
from solo.models import SingletonModel
from films.views import get_last_viewed, list_genres
from films.utils import BlockFilms
from django.utils.translation import ugettext_lazy as _


class IndexView(SingletonModel, TemplateView):
    model = MainPageConfig
    template_name = 'main/page.html'

    def get(self, request, *args, **kwargs):
        films = get_last_viewed()

        return self.render_to_response({
            'films': films,
            'header': _('Collect Films'),
            'config.shell': self.model.get_solo(),
            'list_genres': list_genres(),
            'popular': BlockFilms().films_popular,
            'recommend': BlockFilms().films_recommend,
            'news': BlockFilms().films_news,
            'bestsellers': BlockFilms().films_bestsellers,

            'seo': {
                'title': self.model.get_solo().seo_title,
                'description': self.model.get_solo().seo_description,
                'keywords': self.model.get_solo().seo_keywords
            }
        })
