from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import MainPageConfig
from default_models.admin import SEOPostAdmin


@admin.register(MainPageConfig)
class MainPageConfigAdmin(SingletonModelAdmin, SEOPostAdmin):
    fieldsets = SEOPostAdmin.fieldsets + (
        (None, {
            'fields': (

            ),
        }),
    )

