from qbittorrentapi import Client


def configure_client():
    return Client(
        host='localhost',
        port=7568,
        username='admin',
        password='developer'
    )
