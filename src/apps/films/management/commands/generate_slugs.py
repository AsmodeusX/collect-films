from django.core.management.base import BaseCommand
from films.models import Film, Data
from ...options import CATEGORY_GENRE


class Command(BaseCommand):
    help = 'Generate slugs'

    def handle(self, *args, **options):
        films = Film.objects.filter(slug='').only('title', 'year')
        genres = Data.objects.filter(slug='', category=CATEGORY_GENRE).only('value')

        for film in films:
            try:
                film.slug = Film.slugify('{}-{}'.format(film.title, film.year))
                film.save()

                self.stdout.write(self.style.SUCCESS('Successfully update slug'))
            except Exception as e:
                self.stdout.write(self.style.ERROR('Error: %s' % e))

        for genre in genres:
            try:
                genre.slug = Film.slugify(genre.value)
                genre.save()

                self.stdout.write(self.style.SUCCESS('Successfully update slug'))
            except Exception as e:
                self.stdout.write(self.style.ERROR('Error: %s' % e))

        self.stdout.write(self.style.SUCCESS('Slugs have been updated'))
