from django.core.management.base import BaseCommand, CommandError
from films.models import Film, DataFilm, Data, FilmsPageConfig, RatingFilm, Rating
from django.core.exceptions import ObjectDoesNotExist
import requests, datetime
from films import options as films_options


class Command(BaseCommand):
    help = 'Update films'

    def handle(self, *args, **options):
        films = Film.objects.all()
        imdb_api_key = FilmsPageConfig.get_solo().imdb_api_key

        for film in films:
            if film.imdb:
                film_r = requests.post('http://www.omdbapi.com/?apikey={}&i={}'.format(imdb_api_key, film.imdb), verify=False)
                film_result = film_r.json()
                title = film_result.get('Title')
                year = film_result.get('Year')
                poster = film_result.get('Poster')
                released = film_result.get('Released')
                rated = film_result.get('Rated')
                runtime = film_result.get('Runtime')
                genres = film_result.get('Genre')
                directors = film_result.get('Director')
                writers = film_result.get('Writer')
                actors = film_result.get('Actors')
                plot = film_result.get('Plot')
                languages = film_result.get('Language')
                countries = film_result.get('Country')
                awards = film_result.get('Awards')
                ratings = film_result.get('Ratings')
                website = film_result.get('Website')

                try:
                    date_release = datetime.datetime.strptime(released, '%d %b %Y').date()
                except:
                    date_release = None

                film.title = title
                film.year = year
                film.poster = poster
                film.date_release = date_release
                film.rated = rated
                film.runtime = runtime
                film.plot = plot
                film.awards = awards
                film.website = website

                film.save()

                for genre in str(genres).split(', '):
                    try:
                        data = Data.objects.get(category=films_options.CATEGORY_GENRE, value__iexact=genre)
                    except ObjectDoesNotExist:
                        data = Data.objects.create(
                            category=films_options.CATEGORY_GENRE,
                            value=genre
                        )
                    DataFilm.objects.filter(film_id=film.id).update(
                        data=data
                    )

                for country in str(countries).split(', '):
                    try:
                        data = Data.objects.get(category=films_options.CATEGORY_COUNTRY, value__iexact=country)
                    except ObjectDoesNotExist:
                        data = Data.objects.create(
                            category=films_options.CATEGORY_COUNTRY,
                            value=country
                        )

                    DataFilm.objects.filter(film_id=film.id).update(
                        data=data
                    )

                for director in str(directors).split(', '):
                    try:
                        data = Data.objects.get(category=films_options.CATEGORY_DIRECTOR, value__iexact=director)
                    except ObjectDoesNotExist:
                        data = Data.objects.create(
                            category=films_options.CATEGORY_DIRECTOR,
                            value=director
                        )

                    DataFilm.objects.filter(film_id=film.id).update(
                        data=data
                    )

                for writer in str(writers).split(', '):
                    try:
                        data = Data.objects.get(category=films_options.CATEGORY_WRITER, value__iexact=writer)
                    except ObjectDoesNotExist:
                        data = Data.objects.create(
                            category=films_options.CATEGORY_WRITER,
                            value=writer
                        )

                    DataFilm.objects.filter(film_id=film.id).update(
                        data=data
                    )

                for language in str(languages).split(', '):
                    try:
                        data = Data.objects.get(category=films_options.CATEGORY_LANGUAGE, value__iexact=language)
                    except ObjectDoesNotExist:
                        data = Data.objects.create(
                            category=films_options.CATEGORY_LANGUAGE,
                            value=language
                        )

                    DataFilm.objects.filter(film_id=film.id).update(
                        data=data
                    )

                for actor in str(actors).split(', '):
                    try:
                        data = Data.objects.get(category=films_options.CATEGORY_ACTOR, value__iexact=actor)
                    except ObjectDoesNotExist:
                        data = Data.objects.create(
                            category=films_options.CATEGORY_ACTOR,
                            value=actor
                        )

                    DataFilm.objects.filter(film_id=film.id).update(
                        data=data
                    )

                if ratings:
                    for rating in ratings:
                        source = rating.get('Source')
                        try:
                            current_rating = Rating.objects.get(name=source)
                        except ObjectDoesNotExist:

                            current_rating = Rating.objects.create(
                                name=source
                            )
                        RatingFilm.objects.filter(film_id=film.id).update(
                            rating_id=current_rating.id,
                            value=rating.get('Value')
                        )
            self.stdout.write(self.style.SUCCESS('Successfully update film with id %s') % film.id)
