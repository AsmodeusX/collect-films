from django.core.management.base import BaseCommand
from ...models import Film


class Command(BaseCommand):
    help = 'Clean views'

    def handle(self, *args, **options):
        Film.objects.all().update(
            views_weekly=0
        )

        self.stdout.write(self.style.SUCCESS('Views counter is cleaned'))
