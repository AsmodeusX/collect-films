from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import Film, FilmsPageConfig
from solo.admin import SingletonModelAdmin
from default_models.admin import SEOPostAdmin


@admin.register(FilmsPageConfig)
class FilmsPageConfigAdmin(SingletonModelAdmin, SEOPostAdmin):
    fieldsets = SEOPostAdmin.fieldsets + (
        (None, {
            'fields': (
                'imdb_api_key',
            ),
        }),
        (_('Search page'), {
            'fields': (
                'search_title', 'search_header',
            ),
        }),
        (_('Genres page'), {
            'fields': (
                'genres_title', 'genres_header',
            ),
        }),
    )


@admin.register(Film)
class FilmAdmin(SEOPostAdmin, admin.ModelAdmin):
    fieldsets = SEOPostAdmin.fieldsets + (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'imdb', 'rated', 'year', 'date_release', 'runtime',
                'plot', 'awards', 'poster',  'website', 'video_link', 'trailer',
            ),
        }),
        (_('Information'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'stage', 'views', 'recommend', 'bestseller', 'slug',
            ),
        }),
    )
    list_display = ('title', 'imdb', 'year', 'slug', 'trailer')
    suit_form_tabs = (
        ('general', _('General')),
    )
