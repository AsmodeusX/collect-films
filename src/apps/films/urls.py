from django.urls import path
from . import views, views_ajax

app_name = 'films'
urlpatterns = [
    path('search/', views.SearchView.as_view(), name='search'),
    path('filters/', views_ajax.FiltersView.as_view(), name='filters'),
    path('genres/', views.GenresView.as_view(), name='genres'),
    path('genres/<slug>', views.GenreView.as_view(), name='genre'),
    path('<slug>/', views.FilmView.as_view(), name='film'),
]
