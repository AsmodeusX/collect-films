from django.views.generic import View
from django.template.loader import render_to_string
from django.http import JsonResponse
from .utils import GetFilms
from .models import Film
from . import options


def search_by_params(title, year=None, genre=None):
    films = Film.objects.all()

    if title:
        try:
            films = films.filter(title__icontains=title)
        except:
            pass
    if year:
        try:
            films = films.filter(year__icontains=year)
        except:
            pass
    if genre and genre != 'all':
        try:
            films = films.filter(film_data__data__category=options.CATEGORY_GENRE, film_data__data_id=genre)
        except:
            pass

    return films


class FiltersView(View):
    def get(self, request, *args, **kwargs):
        data = request.GET
        title = data.get('title')

        films = search_by_params(title)

        if films.exists():
            return JsonResponse({
                'films': render_to_string('films/_components/_films.html', {
                    'films': films,
                })
            })
        else:
            GetFilms().get_films(title)
            films = search_by_params(title)
            print(films)

            if films.exists():
                return JsonResponse({
                    'films': render_to_string('films/_components/_films.html', {
                        'films': films,
                    })
                })

        return JsonResponse({
            'message': 'Films Not Found'
        }, status=404)
