from django.utils.translation import ugettext_lazy as _


FILM_STAGE_BASE = 1
FILM_STAGE_DETAIL = 2
FILM_STAGE_DOWNLOAD = 3
FILM_STAGE_FINISHED = 4

FILM_STAGES = (
    (FILM_STAGE_BASE, _('Film searched')),
    (FILM_STAGE_DETAIL, _('Detail searched')),
    (FILM_STAGE_DOWNLOAD, _('Download started')),
    (FILM_STAGE_FINISHED, _('Download finished')),
)
