from django.db import models
from django.utils.translation import ugettext_lazy as _, ugettext
from django.shortcuts import resolve_url
from urllib.error import HTTPError
from solo.models import SingletonModel
from default_models.models import SEOPageConfig
from autoslug import AutoSlugField
from . import conf, options
import re
import requests
import validators
import numpy


class FilmsPageConfig(SingletonModel, SEOPageConfig):
    search_title = models.CharField(_('title'), max_length=128, default='Search Films')
    search_header = models.CharField(_('header'), max_length=128, default='Search Films')
    genres_title = models.CharField(_('title'), max_length=128, default='List of genres')
    genres_header = models.CharField(_('header'), max_length=128, default='List of genres')
    imdb_api_key = models.CharField(_('imdb api key'), max_length=128, default='')

    class Meta:
        default_permissions = ('change', )
        verbose_name = _('settings')

    def __str__(self):
        return ugettext('Home page')


class Rating(models.Model):
    name = models.CharField(_('name'), max_length=128)

    class Meta:
        ordering = ('name', )
        verbose_name = _('Rating')
        verbose_name_plural = _('Ratings')

    def __str__(self):
        return self.name


class Film(SEOPageConfig, models.Model):
    title = models.CharField(_('title'), max_length=255, )
    imdb = models.CharField(_('imdb'), max_length=128, blank=True, null=True)
    rated = models.CharField(_('rated'), max_length=255, blank=True, null=True)
    date_release = models.DateField(_('date release'), blank=True, null=True)
    year = models.CharField(_('year'), blank=True, max_length=64, null=True)
    runtime = models.CharField(_('runtime'), max_length=255, blank=True, null=True)
    plot = models.TextField(_('plot'), blank=True, null=True)
    awards = models.TextField(_('awards'), blank=True, null=True)
    poster = models.URLField(_('poster'), blank=True, null=True)
    website = models.URLField(_('website'), blank=True, null=True)
    video_link = models.TextField(_('video_link'), blank=True)
    trailer = models.URLField(_('trailer'), blank=True)
    # film = models.FileField(_('video'), blank=True)

    stage = models.PositiveSmallIntegerField(_('stage'), choices=conf.FILM_STAGES, default=conf.FILM_STAGE_BASE)
    views = models.PositiveSmallIntegerField(_('views'), default=0)
    views_weekly = models.PositiveSmallIntegerField(_('views weekly'), default=0)
    recommend = models.BooleanField(_('recommend'), default=False)
    bestseller = models.BooleanField(_('bestseller'), default=False)
    date_view = models.DateTimeField(_('latest date view'), blank=True, null=True)
    updated = models.DateTimeField(_('change date'), auto_now=True)
    slug = AutoSlugField(_('slug'), populate_from='title', unique=False, default='', editable=True)

    class Meta:
        default_permissions = ('change', )
        verbose_name = _('Film')
        verbose_name_plural = _('Films')
        ordering = ('-date_release', )

    @staticmethod
    def slugify(text):
        text = text.lower()
        return re.sub(r'[\W_]+', '-', text)

    def get_absolute_url(self):
        return resolve_url('films:film', slug=self.slug)

    def __str__(self):
        return self.title

    def genres(self):
        return self.film_data.filter(data__category=options.CATEGORY_GENRE)

    def directors(self):
        return self.film_data.filter(data__category=options.CATEGORY_DIRECTOR)

    def countries(self):
        return self.film_data.filter(data__category=options.CATEGORY_COUNTRY)

    def writers(self):
        return self.film_data.filter(data__category=options.CATEGORY_WRITER)

    def actors(self):
        return self.film_data.filter(data__category=options.CATEGORY_ACTOR)

    def languages(self):
        return self.film_data.filter(data__category=options.CATEGORY_LANGUAGE)

    def str_genres(self):
        return ', '.join(self.genres().values_list('data__value', flat=True))

    def str_directors(self):
        return ', '.join(self.directors().values_list('data__value', flat=True))

    def str_countries(self):
        return ', '.join(self.countries().values_list('data__value', flat=True))

    def str_writers(self):
        return ', '.join(self.writers().values_list('data__value', flat=True))

    def str_actors(self):
        return ', '.join(self.actors().values_list('data__value', flat=True))

    def str_languages(self):
        return ', '.join(self.languages().values_list('data__value', flat=True))

    def str_search_trailer(self):
        return 'https://www.youtube.com/results?search_query=%s+%s+trailer' % (
            self.title.lower().replace(' ', '+'),
            self.year
        )

    def format_rating(self):
        base_stars = 5
        cnt_ratings = 0
        # 7.6 / 10
        # 81%
        # 64/100
        ratings = self.ratings.values('value')
        if not ratings:
            return None
        list_values = []
        r = 0
        for rating in ratings:
            v = rating.get('value')

            if v.find('%') != -1:
                list_values.append(float(v.replace('%', '')) * base_stars / 100)
            elif v.find('/') != -1:
                parts = v.split('/')

                p1 = float(parts[0])
                p2 = float(parts[1])

                if p2 == 10:
                    p1 = p1 * 10

                list_values.append(float(p1 * base_stars / 100))

        if len(list_values) > 0:
            r = numpy.mean(list_values)

        whole = int(r)
        particular = round((r - whole) * 100)

        return {
            'rating': float("%d.%d" % (whole, particular)),
            'whole': whole,
            'whole_range': range(whole),
            'particular': particular
        }

    def get_trailer(self):
        from django.conf import settings
        from youtube_api import YouTubeDataAPI

        api_key = settings.YT_KEY
        r = []
        try:
            yt = YouTubeDataAPI(api_key)
            r = yt.search(self.str_search_trailer())
        except HTTPError:
            pass
        except Exception as e:
            pass

        if len(r) == 0:
            return None

        film_id = r[0].get('video_id')
        url = "https://www.youtube.com/embed/%s" % film_id

        return url

    def save(self, *args, **kwargs):
        if self.poster:
            if validators.url(self.poster):
                try:
                    r = requests.get(self.poster)

                    if not r.status_code == 200:
                        self.poster = None
                except ConnectionError:
                    print('Cannot connect to host')
                    self.poster = None
                except Exception as e:
                    print(e)
            else:
                self.poster = None

        if not self.trailer:
            trailer = self.get_trailer()

            if trailer:
                self.trailer = trailer

        super(Film, self).save(*args, **kwargs)


class RatingFilm(models.Model):
    rating = models.ForeignKey(Rating, verbose_name=_('rating'), on_delete=models.CASCADE, null=True)
    film = models.ForeignKey(Film, verbose_name=_('film'), related_name='ratings', on_delete=models.CASCADE, null=True)
    value = models.CharField(_('value'), max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = _('Rating Film')
        verbose_name_plural = _('Ratings Film')


class Data(models.Model):
    category = models.PositiveSmallIntegerField(_('category'), choices=options.CATEGORIES, )
    value = models.CharField(_('value'), max_length=255, blank=True, null=True)
    slug = AutoSlugField(_('slug'), populate_from='value', unique=False, default='', editable=True)

    class Meta:
        verbose_name = _('Data Film')
        verbose_name_plural = _('Data Film')

    def link_genre(self):
        if self.category == options.CATEGORY_GENRE:
            return resolve_url('films:genre', slug=self.slug)

    def visible(self):
        return len(self.data_film.only('film')) > 0

    def film_genre(self):
        list_films = []

        if self.category == options.CATEGORY_GENRE:

            # films sorted by rating in genre
            list_ratings = []

            data_films = self.data_film.only('film')
            for data_film in data_films:
                list_ratings.append(data_film.film.format_rating().get('rating'))

            sorted_ratings = sorted(list_ratings)

            for data_film in data_films:
                for sorted_rating in sorted_ratings:
                    if data_film.film.format_rating().get('rating') == sorted_rating:
                        list_films.append(data_film.film)

        if len(list_films) > 0:
            return list_films[0]


class DataFilm(models.Model):
    data = models.ForeignKey(Data, verbose_name=_('data'), related_name='data_film', on_delete=models.CASCADE, default='')
    film = models.ForeignKey(Film, verbose_name=_('film'), related_name='film_data', on_delete=models.CASCADE, default='')

    class Meta:
        verbose_name = _('Data Film')
        verbose_name_plural = _('Data Film')
