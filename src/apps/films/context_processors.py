from .models import Data
from .options import CATEGORY_GENRE


def get_genres(request):
    list_genres = []

    genres = Data.objects.filter(category=CATEGORY_GENRE)

    for genre in genres:
        if genre.visible():
            list_genres.append(genre)

    return {
        'GENRES': list_genres,
    }
