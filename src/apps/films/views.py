from django.views.generic import TemplateView
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.shortcuts import get_object_or_404
from django.core.exceptions import ObjectDoesNotExist
from pyYify import yify
from .models import Film, Data, FilmsPageConfig
from .utils import GetFilms, BlockFilms
from . import options, conf
import qbittorrentapi


def list_genres():
    return Data.objects.filter(category=options.CATEGORY_GENRE)


def get_films_images():
    films = Film.objects.all()
    list_films = []

    for film in films:
        if film.poster:
            list_films.append(film.id)

    return list_films


def get_last_viewed():
    films_ids = get_films_images()
    films = Film.objects.filter(id__in=films_ids).exclude(date_view=None).order_by('-date_view')
    films = films[:9]

    return films


def start_download(film_id, magnet):

    qbt_client = qbittorrentapi.Client(
        host='localhost',
        port=7568,
        username='admin',
        password='developer'
    )
    try:
        qbt_client.auth_log_in()
    except qbittorrentapi.LoginFailed as e:
        return False

    qbt_client.torrents.add(urls=magnet, save_path=settings.MEDIA_ROOT, rename='film_%s' % film_id)
    torrents = qbt_client.torrents.info()

    film = Film.objects.get(id=film_id)

    for torrent in torrents:
        torrent_name = torrent.get('name')

        if torrent_name == 'film_%s' % film_id:
            film.stage = conf.FILM_STAGE_DOWNLOAD
            film.save()

    return film.torrent_hash


class SearchView(TemplateView):
    template_name = 'films/search.html'

    def get(self, request, *args, **kwargs):
        config = FilmsPageConfig.get_solo()
        return self.render_to_response({
            'title': "Search By %s" % config.search_title,
            'header': config.search_header,

            'seo': {
                'title': _('Search by title'),
            }
        })


class GenresView(TemplateView):
    template_name = 'films/genres.html'

    def get(self, request, *args, **kwargs):
        config = FilmsPageConfig.get_solo()

        return self.render_to_response({
            # 'film': film,
            'title': config.genres_title,
            'header': config.genres_header,
            'list_genres': list_genres(),
            'seo': {
                'title': _("Genres"),
            }
        })


class GenreView(TemplateView):
    template_name = 'films/genre.html'

    def get(self, request, slug='', *args, **kwargs):
        genre = get_object_or_404(Data, slug=slug)
        films = Film.objects.filter(film_data__data=genre).distinct()

        return self.render_to_response({
            'films': films,
            'header': genre.value,
            'title': "Genre %s" % genre.value,
            # 'list_genres': list_genres(),
            'seo': {
                'title': genre.value
            }
        })


class FilmView(TemplateView):
    template_name = 'films/film.html'

    def get(self, request, slug=None, *args, **kwargs):
        try:
            film = Film.objects.get(slug=slug)
        except ObjectDoesNotExist:
            return HttpResponseRedirect('/')

        movies_list = None

        try:
            movies_list = yify.search_movies(film.imdb)[0]
        except Exception as e:
            pass

        movies_list = movies_list.torrents if movies_list else []

        film.views += 1
        film.save()

        if film.stage == conf.FILM_STAGE_BASE:
            GetFilms().get_film(film.imdb)

        return self.render_to_response({
            'film': film,
            'movies_list': movies_list,
            'related': BlockFilms().films_related(film.id),
            'header': film.title,
            'seo': {
                'title': film.title,
                'description': film.plot,
                'keywords': film.seo_keywords
            }
        })
