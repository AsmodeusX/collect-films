from django.core.exceptions import ObjectDoesNotExist
from .models import FilmsPageConfig, Film, Data, DataFilm, Rating, RatingFilm
from . import options, conf
import requests


class GetFilms:
    try:
        imdb_api_key = FilmsPageConfig.get_solo().imdb_api_key
    except:
        pass
    url = 'http://www.omdbapi.com'

    def get_films(self, title):
        params = {
            'apikey': self.imdb_api_key,
            's': title.replace(' ', '+'),
            'type': 'movie'
        }
        result = requests.get(url=self.url, params=params, verify=False)

        if result.json().get('Response') == 'False':
            return None

        data = result.json()

        for film in data.get('Search'):
            imdb = film.get('imdbID')
            title = film.get('Title')
            year = film.get('Year')
            poster = film.get('Poster')

            Film.objects.create(
                imdb=imdb,
                title=title,
                year=year,
                poster=poster,
                slug=Film.slugify('{}-{}'.format(title, year))
            )


    def get_film(self, imdb):
        params = {
            'apikey': self.imdb_api_key,
            'i': imdb,
        }
        try:
            result = requests.get(url=self.url, params=params, verify=False)
        except ConnectionError:
            return False
        except:
            return False

        if result.json().get('Response') == 'False':
            return None

        data = result.json()
        films = Film.objects.filter(imdb=imdb)

        if not films.exists():
            return False

        # released = data.get('Released')
        rated = data.get('Rated')
        year = data.get('Year')
        runtime = data.get('Runtime')
        genres = data.get('Genre')
        directors = data.get('Director')
        writers = data.get('Writer')
        actors = data.get('Actors')
        plot = data.get('Plot')
        languages = data.get('Language')
        countries = data.get('Country')
        awards = data.get('Awards')
        ratings = data.get('Ratings')
        website = data.get('Website')

        films.update(
            # date_release=released,
            rated=rated,
            runtime=runtime,
            plot=plot,
            year=year,
            awards=awards,
            website=website,
            stage=conf.FILM_STAGE_DETAIL
        )
        for film in films:
            for genre in genres.split(', '):
                try:
                    data = Data.objects.get(category=options.CATEGORY_GENRE, value__iexact=genre)
                except ObjectDoesNotExist:
                    data = Data.objects.create(
                        category=options.CATEGORY_GENRE,
                        value=genre
                    )
                DataFilm(
                    film=film,
                    data=data
                ).save()

            for country in countries.split(', '):
                try:
                    data = Data.objects.get(category=options.CATEGORY_COUNTRY, value__iexact=country)
                except ObjectDoesNotExist:
                    data = Data.objects.create(
                        category=options.CATEGORY_COUNTRY,
                        value=country
                    )

                DataFilm(
                    film=film,
                    data=data
                ).save()

            for director in directors.split(', '):
                try:
                    data = Data.objects.get(category=options.CATEGORY_DIRECTOR, value__iexact=director)
                except ObjectDoesNotExist:
                    data = Data.objects.create(
                        category=options.CATEGORY_DIRECTOR,
                        value=director
                    )

                DataFilm(
                    film=film,
                    data=data
                ).save()

            for writer in writers.split(', '):
                try:
                    data = Data.objects.get(category=options.CATEGORY_WRITER, value__iexact=writer)
                except ObjectDoesNotExist:
                    data = Data.objects.create(
                        category=options.CATEGORY_WRITER,
                        value=writer
                    )

                DataFilm(
                    film=film,
                    data=data
                ).save()

            for language in languages.split(', '):
                try:
                    data = Data.objects.get(category=options.CATEGORY_LANGUAGE, value__iexact=language)
                except ObjectDoesNotExist:
                    data = Data.objects.create(
                        category=options.CATEGORY_LANGUAGE,
                        value=language
                    )

                DataFilm(
                    film=film,
                    data=data
                ).save()

            for actor in actors.split(', '):
                try:
                    data = Data.objects.get(category=options.CATEGORY_ACTOR, value__iexact=actor)
                except ObjectDoesNotExist:
                    data = Data.objects.create(
                        category=options.CATEGORY_ACTOR,
                        value=actor
                    )

                DataFilm(
                    film=film,
                    data=data
                ).save()

            if ratings:
                for rating in ratings:
                    source = rating.get('Source')
                    try:
                        current_rating = Rating.objects.get(name=source)
                    except ObjectDoesNotExist:

                        current_rating = Rating.objects.create(
                            name=source
                        )
                    RatingFilm(
                        film=film,
                        rating_id=current_rating.id,
                        value=rating.get('Value')
                    ).save()
        print(films)


class BlockFilms:
    count = 7

    def films_popular(self):
        return Film.objects.order_by('-views')[:self.count]

    def films_recommend(self):
        return Film.objects.filter(recommend=True).order_by('?')[:self.count]

    def films_news(self):
        return Film.objects.all()[:self.count]

    def films_bestsellers(self):
        return Film.objects.filter(bestseller=True).order_by('?')[:self.count]

    def films_related(self, film_id):
        return Film.objects.exclude(id=film_id).order_by('?')[:self.count]

