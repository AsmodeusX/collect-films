from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class Config(AppConfig):
    default_auto_field = 'django.db.models.AutoField'
    name = 'films'
    verbose_name = _('Films')

    # def create(cls, entry):
    #
    # def ready(self):
    #     from django.core.management import call_command
    #     from films.models import Film
    #
    #     torrent_hashes = Film.objects.exclude(torrent_hash='').values_list('torrent_hash')
    #     for torrent_hash in torrent_hashes:
    #         call_command('runapscheduler', '--add', '--torrent-hash', torrent_hash)

