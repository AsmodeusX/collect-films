from django.utils.translation import ugettext_lazy as _

CATEGORY_COUNTRY = 1
CATEGORY_GENRE = 2
CATEGORY_DIRECTOR = 3
CATEGORY_WRITER = 4
CATEGORY_ACTOR = 5
CATEGORY_LANGUAGE = 6

CATEGORIES = (
    (CATEGORY_COUNTRY, _('Country')),
    (CATEGORY_GENRE, _('Genre')),
    (CATEGORY_DIRECTOR, _('Director')),
    (CATEGORY_WRITER, _('Writer')),
    (CATEGORY_ACTOR, _('Actor')),
    (CATEGORY_LANGUAGE, _('Language')),
)
