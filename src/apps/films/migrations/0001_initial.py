# Generated by Django 3.2.3 on 2021-07-09 11:46

import autoslug.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Data',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('category', models.PositiveSmallIntegerField(choices=[(1, 'Country'), (2, 'Genre'), (3, 'Director'), (4, 'Writer'), (5, 'Actor'), (6, 'Language')], verbose_name='category')),
                ('value', models.CharField(blank=True, max_length=255, null=True, verbose_name='value')),
                ('slug', autoslug.fields.AutoSlugField(default='', editable=True, populate_from='value', verbose_name='slug')),
            ],
            options={
                'verbose_name': 'Data Film',
                'verbose_name_plural': 'Data Film',
            },
        ),
        migrations.CreateModel(
            name='Film',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('header', models.CharField(default='', max_length=128, verbose_name='header')),
                ('seo_title', models.TextField(blank=True, verbose_name='seo title')),
                ('seo_description', models.TextField(blank=True, verbose_name='seo description')),
                ('seo_keywords', models.TextField(blank=True, verbose_name='seo keywords')),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('imdb', models.CharField(blank=True, max_length=128, null=True, verbose_name='imdb')),
                ('rated', models.CharField(blank=True, max_length=255, null=True, verbose_name='rated')),
                ('date_release', models.DateField(blank=True, null=True, verbose_name='date release')),
                ('year', models.CharField(blank=True, max_length=64, null=True, verbose_name='year')),
                ('runtime', models.CharField(blank=True, max_length=255, null=True, verbose_name='runtime')),
                ('plot', models.TextField(blank=True, null=True, verbose_name='plot')),
                ('awards', models.TextField(blank=True, null=True, verbose_name='awards')),
                ('poster', models.URLField(blank=True, null=True, verbose_name='poster')),
                ('website', models.URLField(blank=True, null=True, verbose_name='website')),
                ('video_link', models.TextField(blank=True, verbose_name='video_link')),
                ('trailer', models.URLField(blank=True, verbose_name='trailer')),
                ('stage', models.PositiveSmallIntegerField(choices=[(1, 'Film searched'), (2, 'Detail searched'), (3, 'Download started'), (4, 'Download finished')], default=1, verbose_name='stage')),
                ('views', models.PositiveSmallIntegerField(default=0, verbose_name='views')),
                ('views_weekly', models.PositiveSmallIntegerField(default=0, verbose_name='views weekly')),
                ('recommend', models.BooleanField(default=False, verbose_name='recommend')),
                ('bestseller', models.BooleanField(default=False, verbose_name='bestseller')),
                ('date_view', models.DateTimeField(blank=True, null=True, verbose_name='latest date view')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='change date')),
                ('slug', autoslug.fields.AutoSlugField(default='', editable=True, populate_from='title', verbose_name='slug')),
            ],
            options={
                'verbose_name': 'Film',
                'verbose_name_plural': 'Films',
                'ordering': ('-date_release',),
                'default_permissions': ('change',),
            },
        ),
        migrations.CreateModel(
            name='FilmsPageConfig',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(default='', max_length=64, verbose_name='title')),
                ('header', models.CharField(default='', max_length=128, verbose_name='header')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='change date')),
                ('seo_title', models.TextField(blank=True, verbose_name='seo title')),
                ('seo_description', models.TextField(blank=True, verbose_name='seo description')),
                ('seo_keywords', models.TextField(blank=True, verbose_name='seo keywords')),
                ('search_title', models.CharField(default='Search Films', max_length=128, verbose_name='search title')),
                ('imdb_api_key', models.CharField(default='', max_length=128, verbose_name='imdb api key')),
            ],
            options={
                'verbose_name': 'settings',
                'default_permissions': ('change',),
            },
        ),
        migrations.CreateModel(
            name='Rating',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128, verbose_name='name')),
            ],
            options={
                'verbose_name': 'Rating',
                'verbose_name_plural': 'Ratings',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='RatingFilm',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.CharField(blank=True, max_length=255, null=True, verbose_name='value')),
                ('film', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ratings', to='films.film', verbose_name='film')),
                ('rating', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='films.rating', verbose_name='rating')),
            ],
            options={
                'verbose_name': 'Rating Film',
                'verbose_name_plural': 'Ratings Film',
            },
        ),
        migrations.CreateModel(
            name='DataFilm',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data', models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, related_name='data_film', to='films.data', verbose_name='data')),
                ('film', models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, related_name='film_data', to='films.film', verbose_name='film')),
            ],
            options={
                'verbose_name': 'Data Film',
                'verbose_name_plural': 'Data Film',
            },
        ),
    ]
