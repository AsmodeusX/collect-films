from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class Config(AppConfig):
    default_auto_field = 'django.db.models.AutoField'
    name = 'subscribes'
    verbose_name = _('Subscribes')
