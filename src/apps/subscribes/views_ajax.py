from django.views.generic import View
from django.template.loader import render_to_string
from django.http import JsonResponse
from .forms import SubscribeForm
from.models import Subscribe
from ajax_views.decorators import ajax_view


@ajax_view('subscribes.subscribe')
class SubscribeView(View):
    def get(self, request, *args, **kwargs):
        form = SubscribeForm()

        return JsonResponse({
            'popup': render_to_string('subscribes/popups/subscribe.html', {
                'form': form
            })
        })

    def post(self, request, *args, **kwargs):
        form = SubscribeForm(request.POST)

        if form.is_valid():
            email_exist = Subscribe.objects.filter(email=form.cleaned_data.get('email'))
            if email_exist.exists():
                email_exist.update(
                    active=True
                )
            else:
                form.save()

            return JsonResponse({
                'popup': render_to_string('subscribes/popups/subscribe_thanks.html')
            })
        else:
            return JsonResponse({
                'errors': form.errors
            }, status=400)
