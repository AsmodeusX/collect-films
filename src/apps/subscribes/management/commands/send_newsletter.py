from django.core.management.base import BaseCommand
from django.contrib.sites.models import Site
import django_rq
from ...models import Subscribe
from ...helpers import send_subscribes


class Command(BaseCommand):
    help = 'Send newsletter'

    def handle(self, *args, **options):
        website = Site.objects.get_current()
        receivers = Subscribe.objects.filter(active=True)

        for receiver in receivers:
            django_rq.enqueue(
                send_subscribes(
                    receiver=receiver,
                    website=website,
                    template='subscribes/mails/newsletter.html',
                ),
            )

        self.stdout.write(self.style.SUCCESS('Messages was sent'))
