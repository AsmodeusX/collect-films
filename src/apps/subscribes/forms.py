from django import forms
from django.utils.translation import ugettext_lazy as _
from .models import Subscribe


class SubscribeForm(forms.ModelForm):
    email = forms.EmailField(
        required=False,
        widget=forms.TextInput(
            attrs={
                'placeholder': 'Email'
            }
        )
    )
    #
    # send_news = forms.BooleanField(
    #     label=_('I also agree to receive newsletter'),
    #     widget=forms.CheckboxInput(
    #         attrs={
    #             'checked': True,
    #         }
    #     )
    # )

    class Meta:
        model = Subscribe
        fields = ['email', ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if not self.cleaned_data.get('email'):
            self.add_error('email', _('Please input an email'))
        return email
