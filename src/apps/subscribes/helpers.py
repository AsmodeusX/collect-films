from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from films.models import Film
import logging


logger = logging.getLogger('rq.worker')


def send_subscribes(receiver, website, template):
    films = Film.objects.order_by("views_weekly")[:10]
    html_message = render_to_string(template, {
        'films': films,
        'website': website,
        'unsubscribe': receiver.unsubscribe,
    })
    plain_message = strip_tags(html_message)

    send_mail(
        subject="Weekly newsletter from CollectFilms",
        message=plain_message,
        from_email="webdevsiteinformation@gmail.com",
        recipient_list=[receiver.email, ],
        fail_silently=False,
        auth_user=None,
        auth_password=None,
        connection=None,
        html_message=html_message
    )
