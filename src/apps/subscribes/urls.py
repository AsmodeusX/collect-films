from django.urls import path
from . import views, views_ajax


app_name = 'subscribe'
urlpatterns = [
    path('ajax/subscribe/', views_ajax.SubscribeView.as_view(), name='subscribe'),
    path('unsubscribe/', views.UnsubscribeView.as_view(), name='unsubscribe'),
]
