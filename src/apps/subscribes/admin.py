from django.contrib import admin
from .models import Subscribe


@admin.register(Subscribe)
class SubscribeAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': (
                'email', 'active',
            ),
        }),
    )

    readonly_fields = ('email', )
    list_editable = ('active', )
    list_display = ('email', 'active', )
    list_filter = ('active', )
    search_fields = ('email', )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
