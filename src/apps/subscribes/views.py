from django.views.generic import TemplateView
from .models import Subscribe


class UnsubscribeView(TemplateView):
    template_name = 'subscribes/unsubscribe.html'

    def get(self, request, mail=None, *args, **kwargs):
        mail = request.GET.get('mail')
        Subscribe.objects.filter(email__iexact=mail).update(
            active=False
        )
        return self.render_to_response({
            'seo': {
                # 'title': film.title,
                # 'description': film.plot,
                # 'keywords': film.seo_keywords
            }
        })
