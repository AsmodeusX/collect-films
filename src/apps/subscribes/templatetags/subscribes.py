from django.template import loader, Library, RequestContext
from ..forms import SubscribeForm

register = Library()


@register.simple_tag(takes_context=True)
def subscribe_block(context):

    form = SubscribeForm()

    return loader.render_to_string('subscribes/subscribe.html', {
        'form': form,
    }, request=context.get('request'))
