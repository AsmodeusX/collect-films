from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.sites.models import Site
from django.shortcuts import resolve_url
from films.models import Film


class Subscribe(models.Model):
    email = models.EmailField(_('email'), )
    active = models.BooleanField(_('active'), default=True)

    class Meta:
        verbose_name = _('Subscriber')
        verbose_name_plural = _('Subscribes')

    def unsubscribe(self):
        website = Site.objects.get_current()

        return "//{}{}?mail={}".format(website, resolve_url('subscribes:unsubscribe'), self.email)
