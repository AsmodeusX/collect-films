from django.contrib.sitemaps import Sitemap, GenericSitemap
from films.models import Film, FilmsPageConfig
from main.models import MainPageConfig


main = {
    'queryset': MainPageConfig.objects.all(),
    'date_field': 'updated',
}
films = {
    'queryset': FilmsPageConfig.objects.all(),
    'date_field': 'updated',
}
films_film = {
    'queryset': Film.objects.all(),
    'date_field': 'updated',
}

sitemaps = {
    'main': GenericSitemap(main, changefreq='daily', priority=1),
    'films': GenericSitemap(films, changefreq='daily', priority=1),
    'films_film': GenericSitemap(films_film, changefreq='daily', priority=1),
}