import os
import environ
import sys


ROOT_DIR = environ.Path(__file__) - 3
BASE_DIR = environ.Path(__file__) - 2

# sys.path.append(os.path.join(ROOT_DIR, 'libs'))
sys.path.insert(0, os.path.join(ROOT_DIR, 'apps_default'))
sys.path.insert(0, os.path.join(ROOT_DIR, 'apps'))
sys.path.insert(0, os.path.join(ROOT_DIR, 'libs'))


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ecpwwwecw8*i&c4ojxuks8srn^umnvai*ur=$a!o9!bj*d4a#+'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [
    'solo',
    'grappelli',
    'ckeditor',
    'admin_honeypot',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'crispy_forms',
    "django_rq",
    'inline_static',

    # 'critical',

    'default_models',
    'main',
    'films',
    'subscribes',
    'custom_scripts',

    'ajax_views',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'settings.urls'
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        "DIRS": (
            str(ROOT_DIR.path("templates")),
        ),
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'films.context_processors.get_genres'
            ],
        },
    },
]
CRISPY_TEMPLATE_PACK = 'bootstrap4'

WSGI_APPLICATION = 'settings.wsgi.application'
ASGI_APPLICATION = "settings.asgi.application"

ADMINS = (
    ('dev', 'klinov.mikhail@inbox.ru'),
)
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'webdevsiteinformation@gmail.com'
EMAIL_HOST_PASSWORD = 'ewoujxzemwnvzbji'
DEFAULT_FROM_EMAIL = SERVER_EMAIL = EMAIL_HOST_USER
EMAIL_PORT = 465
EMAIL_USE_SSL = True
EMAIL_USE_TLS = False


DATABASES = {}


AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'
DATA_UPLOAD_MAX_NUMBER_FIELDS = 10240


LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Media
MEDIA_ROOT = os.path.abspath(os.path.join(ROOT_DIR, '..', 'media'))
MEDIA_URL = '/media/'

STATIC_ROOT = os.path.abspath(os.path.join(ROOT_DIR, '..', 'static'))

# Static files (CSS, JavaScript, Images)
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(ROOT_DIR, 'dist'),
)

STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.ManifestStaticFilesStorage'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
]

SITE_ID = 1
CKEDITOR_UPLOAD_PATH = "uploads/"
CKEDITOR_IMAGE_BACKEND = "pillow"
CKEDITOR_JQUERY_URL = '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'

CKEDITOR_CONFIGS = {
    "default": {
        "removePlugins": "stylesheetparser",
        'allowedContent': True,
        'toolbar_Full': [
        ['Styles', 'Format', 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ],
        ['Image', 'Flash', 'Table', 'HorizontalRule'],
        ['TextColor', 'BGColor'],
        ['Smiley','sourcearea', 'SpecialChar'],
        [ 'Link', 'Unlink', 'Anchor' ],
        [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ],
        [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ],
        [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
        [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ],
        [ 'Maximize', 'ShowBlocks' ]
    ],
    }
}

# Redis Cache
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient"
        },
        "KEY_PREFIX": SECRET_KEY
    }
}

# Channels Redis
CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": [("127.0.0.1", 6379)],
        },
    },
}

YT_KEY = "AIzaSyDyyIYwDlblslLltsa-M9E6r7-GHVhw7Rc"


RQ_QUEUES = {
    'default': {
        'HOST': 'localhost',
        'PORT': 6379,
        'DB': 0,
        'DEFAULT_TIMEOUT': 360,
    },
}


# try:
#     import env_file
#     env_file.load(str(os.path.join(ROOT_DIR, '..', '.env')))
# except Exception as e:
#     print(e)
try:
    import env_file

    variables = env_file.get(str(os.path.join(ROOT_DIR, '..', '.env')))
except Exception as e:
    pass
