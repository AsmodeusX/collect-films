from settings.configs.base import *

DEBUG = True

# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

DOMAIN = '127.0.0.1'

EMAIL_PORT = 587
EMAIL_USE_SSL = False
EMAIL_USE_TLS = True

ALLOWED_HOSTS = (
    DOMAIN,
    'localhost'
)
DATABASES.update({
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'collect_films',
        'USER': 'dev',
        'PASSWORD': 'dev',
        'HOST': '127.0.0.1',
    }
})

