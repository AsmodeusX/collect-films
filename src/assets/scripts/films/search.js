(function ($) {
    var clVisible = 'visible';

    $(document).ready(function () {
        var $btnSearch = $('.btnSearch');

        $btnSearch.on('click', function (e) {
            e.preventDefault();
            var $formSearch = $(this).closest('.formSearch');
            var $textSearch = $formSearch.find('.textSearch');
            if ($formSearch.hasClass(clVisible)) {
                if ($textSearch.val().trim().length !== 0) {
                    $formSearch.submit();
                }
            } else {
                $formSearch.addClass(clVisible);
                $textSearch.focus();
            }
        });
        // $("#filterYear").selectmenu({
        //     appendTo: "#field-search-year"
        // });

        var $searchElem = $('#search-page');
        if ($searchElem.length)
            var app_filters = new Vue({
                el: '#search-page',
                data: {
                    title: findGetParameter('search')
                },
                methods: {
                    sendForm: function (e) {
                        e.preventDefault();
                        return false;
                    },
                    updateFilms: function (e) {
                        getMovies(this.title);
                    }
                },
                mounted() {
                    getMovies(this.title);

                }
            });
    })

    $(document).mouseup(function (e){ // событие клика по веб-документу
        var $form = $('.formSearch');
        if (!$form.is(e.target) // если клик был не по нашему блоку
            && $form.has(e.target).length === 0) { // и не по его дочерним элементам
            if ($form.find('input').eq(0).val().trim().length === 0) {
                $form.removeClass(clVisible);
                $form.find('input').blur();
            }
        }
    });

    function getMovies(title) {
        $('.search-information').removeClass('search-information--hidden');
        $('.search-preloader').removeClass('hidden');
        $('.search-error').addClass('hidden');
        $('.films .row').html('');

        axios.get('/films/filters/', {
            params: {
                title: title
            }
        })
            .then((response) => {
                $('.search-information').addClass('search-information--hidden');
                $('.search-preloader').addClass('hidden');
                $('.search-error').addClass('hidden');
                $('.films .row').html(response.data.films);
            }, (error) => {
                $('.search-preloader').addClass('hidden');
                $('.search-error').removeClass('hidden');
                console.error(error);
            });
    }

    function findGetParameter(parameterName) {
        var result = null,
            tmp = [];
        location.search
            .substr(1)
            .split("&")
            .forEach(function (item) {
                tmp = item.split("=");
                if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]).replace('+', ' ');
            });
        return result;
    }
})(jQuery);