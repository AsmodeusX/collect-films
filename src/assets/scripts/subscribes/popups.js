(function ($) {
    $(document).on('click', '.open-popup-subscribe', function (e) {
        e.preventDefault();
        var film_id = $(e.target).data('film');
        getPopup(film_id);
    });

    function getPopup(film_id) {
        axios.get('/subscribes/ajax/subscribe/', {
            params: {
                film_id: film_id
            },
        })
            .then((response) => {
                $.basePopup({
                    closeOnOtherClick: true,
                    addBtnClose: true,
                    content: response.data.popup,
                    classes: 'popup--subscribe'
                });
            }, (error) => {
                console.error(error);
            });
    }
})(jQuery);