(function ($) {
    var mainPath = '/';

    $.checkHash = function () {
        var hash = location.hash.toLowerCase(),
            path = window.location.pathname;

        path === mainPath ? getBlockByHash(hash) : redirectToMain(hash);
    }

    function getBlockByHash(hash) {
        var format_hash = hash.replace('#', '');

        if (hash) {
            var $elem = $('.block-films--' + format_hash).eq(0);

            if ($elem && $elem.length > 0) {
                var t = $elem.offset().top;
                var $menu = $('.navbar.navbar--fixed');
                if ($menu && $menu.length) t -= $menu.eq(0).innerHeight();

                $("html, body").animate({ scrollTop: t }, 2000);
            }
        }
    }

    function redirectToMain(hash) {
        window.location.href = mainPath + hash;
    }
})(jQuery);
