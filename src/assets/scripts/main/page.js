(function ($) {
    $(document).ready(function () {

        var swiper = new Swiper('.swiper-container', {
            loop: true,
            slidesPerView: 1,
            slidesPerGroup: 1,
            effect: 'fade',
            fadeEffect: {
                crossFade: true
            },
            speed: 2000,
            autoplay: {
                delay: 5000,
            },
            allowTouchMove: false,
        });
    });

    $(window).on('load', function () {
        $.checkHash();

    });
})(jQuery);