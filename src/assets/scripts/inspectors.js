(function($) {
    var checkElements = {
        'visibility': []
    }

    $.AddElementCheckVisibility = function(elem) {
        var elemArr = {
            element: elem,
            visible: false,
        }
        checkElements.visibility.push(elemArr)
    }

    $.stateVisibility = function(elem) {
        var curIndex;
        checkElements.visibility.forEach(function (item, index) {
            var arrElem = item.element;
            if (elem === arrElem) {
                curIndex = index;
                return false;
            }
        })
        return checkElements.visibility[curIndex].visible;
    }

    $(window).on('load scroll', function () {
        checkVisibilityElement();
    })

    function checkVisibilityElement () {
        checkElements.visibility.forEach(function (item, index) {
            var $elem = $(item.element);
            var scrollTop = $(window).scrollTop();
            var elemTop = $elem.offset().top,
                elemBottom = elemTop + $elem.innerHeight();
            checkElements.visibility[index].visible = elemTop >= scrollTop || elemBottom >= scrollTop;
        })
    }
})(jQuery);