**1. Install Django**

        1. Run "`python3 -m venv env`"
        2. Run "`source env\Scripts\activate`"
        3. Run "`pip3 install -r requirements/dev.txt`"
        4. Run "`cd src`"
        5. Run "`python3 manage.py migrate`"
        6. Run "`python3 manage.py collectstatic --noi`"
        7. Run "`python3 manage.py createsuperuser`"

**2. Collect Staticfiles**

        1. Run "`npm install -g gulp`"
        2. Run "`npm install -g bower`"
        3. Run "`npm install`"
        4. Run "`bower install`"

**3. Install Redis**

        1. Run "`docker-compose up -d`"

**4. Build Project**

        1. Run "`gulp build`"